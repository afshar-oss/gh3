
import pytest

import gh3


def test_add_route():
  def text_target(ctx):
    ctx.reply_text('hello')
  app = gh3.App()
  app.add_route('/', text_target)
  assert '/' == app.routes[0].rule
  assert 'text_target' == app.routes[0].endpoint
  assert text_target == app.targets['text_target']


def test_readd_route():
  def text_target(ctx):
    ctx.reply_text('hello')
  app = gh3.App()
  app.add_route('/', text_target)
  app.add_rule(gh3.Rule('/name', endpoint='text_target'))
  assert 'text_target' == app.routes[0].endpoint
  assert 'text_target' == app.routes[1].endpoint


def test_finalize():
  def text_target(ctx):
    ctx.reply_text('hello')
  app = gh3.App()
  app.add_route('/', text_target)
  assert None is app.route_map 
  app.finalize()
  assert '/' == list(app.route_map.iter_rules())[0].rule
  assert 'text_target' == list(app.route_map.iter_rules())[0].endpoint


def test_named_endpoint():
  def text_target(ctx):
    ctx.reply_text('hello')
  app = gh3.App()
  app.add_route('/', text_target, 'named_endpoint')
  assert '/' == app.routes[0].rule
  assert 'named_endpoint' == app.routes[0].endpoint
  assert text_target == app.targets['named_endpoint']


def test_text_target():
  def text_target(ctx):
    ctx.reply_text('hello')
  app = gh3.App()
  app.add_route('/', text_target)
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_endpoint_args():
  def text_target(ctx):
    user = ctx.endpoint_args['user']
    ctx.reply_text(f'hello {user}')
  app = gh3.App()
  app.add_route('/<user>', text_target)
  resp = app.tester().get('/ali')
  assert 200 == resp.status_code
  assert b'hello ali' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_url():
  def text_target(ctx):
    u = ctx.url('text_target')
    ctx.reply_text(f'hello {u}')
  app = gh3.App()
  app.add_route('/', text_target)
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello /' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type

def test_url_args():
  def text_target(ctx):
    u = ctx.url('text_target', {'arg': 'banana'})
    ctx.reply_text(f'hello {u}')
  app = gh3.App()
  app.add_route('/<arg>', text_target)
  resp = app.tester().get('/banana')
  assert 200 == resp.status_code
  assert b'hello /banana' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_url_args_extra():
  def text_target(ctx):
    u = ctx.url('text_target', {'arg': 'banana', 'moo': 'c'})
    ctx.reply_text(f'hello {u}')
  app = gh3.App()
  app.add_route('/<arg>', text_target)
  resp = app.tester().get('/b')
  assert 200 == resp.status_code
  assert b'hello /banana?moo=c' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_url_canonical():
  def text_target(ctx):
    u = ctx.url('text_target', canonical=True)
    ctx.reply_text(f'hello {u}')
  app = gh3.App()
  app.add_route('/', text_target)
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello http://localhost/' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_error():
  def error_target(ctx):
    ctx.reply_error(401)
  app = gh3.App()
  app.add_route('/', error_target)
  resp = app.tester().get('/')
  assert 401 == resp.status_code
  assert b'401 UNAUTHORIZED' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_html():
  def html_target(ctx):
    ctx.reply_html('<h1>yo!</h1>')
  app = gh3.App()
  app.add_route('/', html_target)
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'<h1>yo!</h1>' == resp.get_data()
  assert 'text/html; charset=utf-8' == resp.content_type


def test_json():
  def json_target(ctx):
    ctx.reply_json({'hello':'world'})
  app = gh3.App()
  app.add_route('/', json_target)
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'{"hello": "world"}' == resp.get_data()
  assert 'application/json' == resp.content_type


def test_notfound():
  app = gh3.App()
  resp = app.tester().get('/')
  assert 404 == resp.status_code
  assert b'404 NOT FOUND' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_double_finalize():
  app = gh3.App()
  assert True == app.finalize()
  assert False == app.finalize()


def test_route_plugin():
  def text_target(ctx):
    ctx.reply_text('hello')
  class RoutePlugin(gh3.Plugin):
    def add_routes(self, app):
      app.add_route('/', text_target)
  app = gh3.App()
  app.add_plugin(RoutePlugin())
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_addplugin_plugin():
  def text_target(ctx):
    ctx.reply_text('hello')
  class RoutePlugin(gh3.Plugin):
    def add_routes(self, app):
      app.add_route('/', text_target)
  class AdderPlugin(gh3.Plugin):
    def add_plugins(self, app):
      app.add_plugin(RoutePlugin())
  app = gh3.App()
  app.add_plugin(AdderPlugin())
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_before_plugin():
  def banana_target(ctx):
    ctx.reply_text(ctx.banana)
  class BeforePlugin(gh3.Plugin):
    def before_request(self, ctx):
      ctx.banana = 'I am a banana, obviously'
  app = gh3.App()
  app.add_route('/', banana_target)
  app.add_plugin(BeforePlugin())
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'I am a banana, obviously' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_after_plugin():
  def text_target(ctx):
    ctx.reply_text('hello')
  class AfterPlugin(gh3.Plugin):
    def after_request(self, ctx):
      ctx.resp.set_data(ctx.resp.get_data().decode('utf-8') + ' world')
  app = gh3.App()
  app.add_route('/', text_target)
  app.add_plugin(AfterPlugin())
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello world' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_ctx_decorating_plugin():
  class DecoPlugin(gh3.Plugin):
    banana = 'banana'
    decorate_context_as = 'mydeco'
  plugin = DecoPlugin()
  def text_target(ctx):
    assert plugin is ctx.mydeco
    ctx.reply_text(f'hello {ctx.mydeco.banana}')
  app = gh3.App()
  app.add_route('/', text_target)
  app.add_plugin(plugin)
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello banana' == resp.get_data()
  assert 'text/plain; charset=utf-8' == resp.content_type


def test_charset():
  def latin1_target(ctx):
    ctx.resp_charset('latin-1')
    ctx.reply_text('hello')
  def latin2_target(ctx):
    ctx.resp_charset('latin-1')
    ctx.reply_html('hello')
  app = gh3.App()
  app.add_route('/t', latin1_target)
  app.add_route('/h', latin2_target)
  resp = app.tester().get('/t')
  assert 200 == resp.status_code
  assert b'hello' == resp.get_data()
  assert 'text/plain; charset=latin-1' == resp.content_type
  resp = app.tester().get('/h')
  assert 200 == resp.status_code
  assert b'hello' == resp.get_data()
  assert 'text/html; charset=latin-1' == resp.content_type


def test_response_type():
  class Latin1Response(gh3.Response):
    charset = 'latin-1'
  def text_target(ctx):
    ctx.reply_text('hello')
  app = gh3.App()
  app.response_type = Latin1Response
  app.add_route('/', text_target)
  resp = app.tester().get('/')
  assert 200 == resp.status_code
  assert b'hello' == resp.get_data()
  assert 'text/plain; charset=latin-1' == resp.content_type
  

def test_restfulplugin():
  class Api(gh3.ApiPlugin):
    service_name = 'api'
    service_path = '/api'
    def on_get(self, ctx):
      ctx.reply_json({'hello': 'world'})
    def on_post(self, ctx):
      ctx.reply_json({'hello': 'post'})
    def on_patch(self, ctx):
      ctx.reply_json({'hello': 'patch'})
    def on_delete(self, ctx):
      ctx.reply_json({'hello': 'delete'})
  app = gh3.App()
  app.add_plugin(Api())
  resp = app.tester().get('/api')
  assert 200 == resp.status_code
  assert 'application/json' == resp.content_type
  assert {'hello': 'world'} == resp.get_json()
  resp = app.tester().post('/api')
  assert 200 == resp.status_code
  assert 'application/json' == resp.content_type
  assert {'hello': 'post'} == resp.get_json()
  resp = app.tester().patch('/api')
  assert 200 == resp.status_code
  assert 'application/json' == resp.content_type
  assert {'hello': 'patch'} == resp.get_json()
  resp = app.tester().delete('/api')
  assert 200 == resp.status_code
  assert 'application/json' == resp.content_type
  assert {'hello': 'delete'} == resp.get_json()


def test_restful_missingname():
  class Api(gh3.ApiPlugin):
    pass
  app = gh3.App()
  app.add_plugin(Api())
  with pytest.raises(ValueError):
    app.finalize()
  class Api(gh3.ApiPlugin):
    service_name = 'banana'
  app = gh3.App()
  app.add_plugin(Api())
  with pytest.raises(ValueError):
    app.finalize()
  class Api(gh3.ApiPlugin):
    service_path = '/banana'
  app = gh3.App()
  app.add_plugin(Api())
  with pytest.raises(ValueError):
    app.finalize()

# vim: ft=python sw=2 ts=2 sts=2 tw=80
