
test: ve
	./ve/bin/pytest --cov=gh3 --cov-report=term-missing

docs: ve
	cp README.md dev/docs/source/
	./ve/bin/sphinx-build dev/docs/source dev/docs/build

clean-docs:
	rm -rf docs/build/*

serve-docs: docs
	cd dev/docs && firebase serve --only=hosting

upload-docs: clean-docs docs
	cd dev/docs && firebase deploy


ve:
	virtualenv -p python3.9 ve
	./ve/bin/pip install -r dev/deps.txt
	./ve/bin/pip install -r dev/devdeps.txt

clean-ve:
	rm -rf ve

clean:
	rm -rf __pycache__ .coverage .pytest_cache dist */*/__pycache__

copyproj:
	git mv dev/pyproject.toml .

uncopyproj:
	git mv pyproject.toml dev/

flitbuild: ve copyproj
	./ve/bin/flit build || make uncopyproj

flitpublish: ve copyproj
	./ve/bin/flit publish || make uncopyproj
