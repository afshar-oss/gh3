
gh3 API
=======

.. automodule:: gh3

.. Note::
  See also: the Werkzeug primitives used by gh3:

  * :py:class:`werkzeug.wrappers.Response`
  * :py:class:`werkzeug.wrappers.Request`

.. autoclass:: gh3.App
  :members:
  :undoc-members:

.. autoclass:: gh3.Context
  :members:
  :undoc-members:

.. autoclass:: gh3.RequestTarget
  :members:
  :undoc-members:

.. autoclass:: gh3.Plugin
  :members:
  :undoc-members:

.. autoclass:: gh3.ApiPlugin
  :members:
  :undoc-members:

