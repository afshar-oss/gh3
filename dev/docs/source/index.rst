.. gh3 documentation master file, created by
   sphinx-quickstart on Sun Dec  5 18:50:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gh3's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Quickstart <README.md>
   API <api.rst>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
